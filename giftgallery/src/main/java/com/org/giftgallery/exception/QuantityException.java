package com.org.giftgallery.exception;

public class QuantityException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918173375418671210L;

	public QuantityException(String message) {
		super(message);
	}

}
