package com.org.giftgallery.exception;

public class ProductException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5918173375418671210L;

	public ProductException(String message) {
		super(message);
	}

}
