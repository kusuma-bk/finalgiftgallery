package com.org.giftgallery.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CartEditRequestDto {
	private int quantity;
	private long cartId;

	private List<RecipientRequestListDto> recipientDetails;
}
