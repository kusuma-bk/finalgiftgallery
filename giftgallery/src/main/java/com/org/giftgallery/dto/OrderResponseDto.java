package com.org.giftgallery.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderResponseDto {
	private String message;
	private int status;

}
