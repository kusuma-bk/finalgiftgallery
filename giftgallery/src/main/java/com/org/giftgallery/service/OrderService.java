package com.org.giftgallery.service;

import javax.mail.MessagingException;

import com.org.giftgallery.dto.OrderResponseDto;
import com.org.giftgallery.exception.CustemerTypeExceptionForCorperator;
import com.org.giftgallery.exception.CustemerTypeExceptionForPersonal;
import com.org.giftgallery.exception.OrderListNotPresent;

public interface OrderService {


	OrderResponseDto order(long cartId)
			throws CustemerTypeExceptionForPersonal, OrderListNotPresent, CustemerTypeExceptionForCorperator, MessagingException;

	String sendMail(long cartId) throws MessagingException;

}
