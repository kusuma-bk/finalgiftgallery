package com.org.giftgallery.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.org.giftgallery.dto.OrderResponseDto;
import com.org.giftgallery.entity.Cart;
import com.org.giftgallery.entity.Order;
import com.org.giftgallery.entity.Recipient;
import com.org.giftgallery.entity.User;
import com.org.giftgallery.exception.CustemerTypeExceptionForCorperator;
import com.org.giftgallery.exception.CustemerTypeExceptionForPersonal;
import com.org.giftgallery.exception.OrderListNotPresent;
import com.org.giftgallery.repository.CartRepository;
import com.org.giftgallery.repository.OrderRepository;
import com.org.giftgallery.repository.RecipientRepository;
import com.org.giftgallery.repository.UserRepository;
import com.org.giftgallery.utility.CustomerUtility;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private JavaMailSender sender;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	RecipientRepository recipientRepository;

	@Autowired
	CartRepository cartRepository;

	@Autowired
	UserRepository userRepository;

	@Override
	public String sendMail(long cartId) throws MessagingException {

		MimeMessage message = sender.createMimeMessage();
		System.out.println(message);
		MimeMessageHelper helper = new MimeMessageHelper(message);
		Optional<Cart> optionalCart = cartRepository.findById(cartId);
		
		List<Recipient> listOfRecipient = recipientRepository.findByCartId(cartId);
		Optional<User> optionalOfUser = userRepository.findById(optionalCart.get().getUserId());
	
		

		for (Recipient recipient : listOfRecipient) {
			helper.setTo(recipient.getRecipientEmail());
			helper.setBcc(optionalOfUser.get().getEmailId());
			helper.setText(recipient.getMessage());
			helper.setSubject("Greetings From Gift Gallery :)......");
			sender.send(message);
		}
		return "Mail Sent Success!";

	}

	@Override
	public OrderResponseDto order(long cartId) throws CustemerTypeExceptionForPersonal, OrderListNotPresent,
			CustemerTypeExceptionForCorperator, MessagingException {

		List<Recipient> listOfRecipients = recipientRepository.findByCartId(cartId);
		Optional<Cart> cart = cartRepository.findByCartId(cartId);
		if (!cart.isPresent()) {
			throw new OrderListNotPresent(CustomerUtility.ORDER_NOT_FOUNT);
		}
		if (cart.get().getUserType().equalsIgnoreCase("personal")) {
			if (listOfRecipients.size() > 3) {
				throw new CustemerTypeExceptionForPersonal(CustomerUtility.CUSTOMER_TYPE_EXCEPTION_FOR_PERSONAL);
			}
		}

		if (cart.get().getUserType().equalsIgnoreCase("corporate")) {
			if (listOfRecipients.size() <= 5) {
				throw new CustemerTypeExceptionForCorperator(CustomerUtility.CUSTOMER_TYPE_EXCEPTION_FOR_CORPERATE);
			}
		}

		Order order = new Order();
		order.setCartId(cartId);
		order.setDate(LocalDateTime.now());
		Order orderPlaced = orderRepository.save(order);

		OrderResponseDto orderResponseDto = new OrderResponseDto();
		orderResponseDto.setMessage("your order is comfirmed");
		orderResponseDto.setStatus(613);
		sendMail(orderPlaced.getCartId());
		return orderResponseDto;
	}

}
