package com.org.giftgallery.controllertest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.org.giftgallery.controller.CartController;
import com.org.giftgallery.dto.CartEditRequestDto;
import com.org.giftgallery.dto.CartResponseDto;
import com.org.giftgallery.dto.EditResponseDto;
import com.org.giftgallery.dto.RecipientRequestListDto;
import com.org.giftgallery.entity.Cart;
import com.org.giftgallery.exception.CartIdNotPresent;
import com.org.giftgallery.service.CartService;

@RunWith(MockitoJUnitRunner.class)
public class CartControllerTest {
	@Mock
	CartService cartService;

	@InjectMocks
	CartController cartController;

	CartResponseDto cartResponseDto;
	CartEditRequestDto cartEditRequestDto;
	EditResponseDto editResponseDto;
	List<RecipientRequestListDto> listOfrecipients;
	Cart cart;
	RecipientRequestListDto recipientRequestListDto;
	long userId;

	@Before
	public void setUp() {
		userId = 1L;
		cart = new Cart();
		cart.setCartId(1);
		cart.setProductId(1);
		cart.setQuantity(5);
		cart.setUserId(1);
		cart.setUserType("corporate");

		recipientRequestListDto = new RecipientRequestListDto();
		recipientRequestListDto.setRecipientName("kusuma");
		recipientRequestListDto.setRecipientEmail("kusuma@gmail.com");
		recipientRequestListDto.setRecipientId(1);
		recipientRequestListDto.setRecipientPhoneNumber("48957575");
		recipientRequestListDto.setMessage("greetings from pallavi");

		listOfrecipients = new ArrayList<RecipientRequestListDto>();
		listOfrecipients.add(recipientRequestListDto);

		cartEditRequestDto = new CartEditRequestDto();
		cartEditRequestDto.setCartId(1);
		cartEditRequestDto.setQuantity(4);

		editResponseDto = new EditResponseDto();
		editResponseDto.setMessage("updated success");
		editResponseDto.setStatus(680);

		cartResponseDto = new CartResponseDto();
		cartResponseDto.setCartId(1);
		cartResponseDto.setProductName("coffe mug");
		cartResponseDto.setQuantity(5);
		cartResponseDto.setUserId(1);
		cartResponseDto.setUserType("corporate");
		cartResponseDto.setRecipientDetails(listOfrecipients);
		cartResponseDto.setStatus(630);
	}

	
	@Test
	public void updateCartDetails() {
		Mockito.when(cartService.updateCartDetails(Mockito.any())).thenReturn(editResponseDto);
		ResponseEntity<EditResponseDto> response = cartController.updateCartDetails(cartEditRequestDto);
		Assert.assertEquals(editResponseDto, response.getBody());
	}

	@Test
	public void cartDetails() throws CartIdNotPresent {
		Mockito.when(cartService.cartDetails(Mockito.anyLong())).thenReturn(cartResponseDto);
		ResponseEntity<CartResponseDto> response = cartController.cartDetails(userId);
		Assert.assertEquals(cartResponseDto, response.getBody());
	}
}
